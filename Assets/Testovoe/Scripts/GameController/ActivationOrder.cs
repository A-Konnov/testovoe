﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ActivationOrder : MonoBehaviour
{
    private int[] m_serialNumber;

    private void Start()
    {
        m_serialNumber = new int[gameObject.transform.childCount];

        for (int i = 0; i < m_serialNumber.Length; i++)
        {
            m_serialNumber[i] = gameObject.transform.GetChild(i).GetComponent<AObelisk>().SerialNumber;
        }
    }

    private void Update()
    {
        if (!InOrder())
        {

            foreach (Transform child in gameObject.transform)
            {
                child.GetComponent<AObelisk>().IsActivate = false;
            }

            var fireworks = GameObject.FindGameObjectsWithTag("Fireworks of happiness");

            foreach (GameObject firework in fireworks)
            {
                Destroy(firework);
            }

            foreach (Transform child in gameObject.transform)
            {
                child.GetComponent<AObelisk>().IsReset = true;
            }
        }

        if (AllActivate())
        {
            StartCoroutine(Restart());
        }

    }

    private IEnumerator Restart()
    {
        var delay = new WaitForSeconds(5);
        yield return delay;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    private bool AllActivate()
    {
        foreach (Transform child in gameObject.transform)
        {
            if (child.GetComponent<AObelisk>().IsActivate == false)
            {
                return false;
            }
        }
        return true;
    }

    private bool InOrder()
    {
        for (int i = 0; i < m_serialNumber.Length; i++)
        {
            if (gameObject.transform.GetChild(i).GetComponent<AObelisk>().IsActivate == true)
            {
                for (int j = 0; j < m_serialNumber.Length; j++)
                {
                    if (m_serialNumber[j] < m_serialNumber[i] && gameObject.transform.GetChild(j).GetComponent<AObelisk>().IsActivate == false)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
  
}
