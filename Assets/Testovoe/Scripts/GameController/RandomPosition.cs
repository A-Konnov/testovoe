﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomPosition
{
    private static float maxPozX;
    private static float maxPozZ;
    private static float m_pozX;
    private static float m_pozZ;

    public static Vector3 GetPosition(float maxPozX, float maxPozZ)
    {
        m_pozX = Random.Range(-maxPozX, maxPozX);
        m_pozZ = Random.Range(-maxPozZ, maxPozZ);
        Vector3 position = new Vector3(m_pozX, 0, m_pozZ);

        return position;        
    }
}
