﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawn : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField] GameObject[] m_obelisk;
    [SerializeField] Image[] m_icons;
    [SerializeField] Sprite[] m_sprites;

    [Header("Property")]
    [SerializeField] private float m_maxPozX = 50;
    [SerializeField] private float m_maxPozZ = 50;
    [SerializeField] private float m_distance = 5;

    private int[] m_serialNumber = { 0, 1, 2 };
    private GameObject m_player;
    private float m_sqrDistance;

    private void Awake()
    {
        SpawnPlayer();

        m_serialNumber = ShuffleArray(m_serialNumber);
        m_sqrDistance = m_distance * m_distance;

        InstallObelisks();
        AssignActivationOrder();
    }

    private void SpawnPlayer()
    {
        m_player = GameObject.FindGameObjectWithTag("Player");
        m_player.transform.position = RandomPosition.GetPosition(m_maxPozX, m_maxPozZ);
    }

    private void InstallObelisks()
    {
        for (int i = 0; i < m_obelisk.Length;)
        {
            if (NotAtOnePoint())
            {
                Instantiate(m_obelisk[i], RandomPosition.GetPosition(m_maxPozX, m_maxPozZ), Quaternion.identity, gameObject.transform);
                i++;
            }
        }
    }

    private void AssignActivationOrder()
    {
        for (int i = 0; i < m_obelisk.Length; i++)
        {
            gameObject.transform.GetChild(i).GetComponent<AObelisk>().SerialNumber = m_serialNumber[i];
            m_icons[m_serialNumber[i]].GetComponent<Image>().sprite = m_sprites[i];
        }
    }

    private int[] ShuffleArray(int[] numbers)
    {
        int[] newArray = numbers.Clone() as int[];
        for (int i = 0; i < newArray.Length; i++)
        {
            int tmp = newArray[i];
            int r = Random.Range(i, newArray.Length);
            newArray[i] = newArray[r];
            newArray[r] = tmp;
        }
        return newArray;
    }

    private bool NotAtOnePoint()
    {
        var position = RandomPosition.GetPosition(m_maxPozX, m_maxPozZ);
        var distanceToPlayer = (m_player.transform.position - position).sqrMagnitude;
        if (distanceToPlayer < m_sqrDistance)
        {
            return false;
        }
        foreach (GameObject obelisk in m_obelisk)
        {
            var distanceToObelisk = (obelisk.transform.position - position).sqrMagnitude;
            if (distanceToObelisk < m_sqrDistance)
            {
                return false;
            }
        }
        return true;
    }
}
