﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AObelisk : MonoBehaviour
{
    [SerializeField] private GameObject m_fireworks;
    [SerializeField] private GameObject m_cloud;
    [SerializeField] private Transform m_spawnParticle;

    public int SerialNumber { get; set; }
    public bool IsActivate { get; set; }
    public bool IsReset { get; set; }

    private void Start()
    {
        IsActivate = false;
        IsReset = false;
    }

    private void Update()
    {
        if (IsActivate)
        {
            ActivateFireworks();
        }

        if (IsReset)
        {
            ActivationCloud();
        }
    }

    private void ActivationCloud()
    {
        if (gameObject.transform.childCount <= 2)
        {
            var cloud = Instantiate(m_cloud, m_spawnParticle.position, m_spawnParticle.rotation, gameObject.transform);
            IsReset = false;
            Destroy(cloud, 3);
        }
    }

    private void ActivateFireworks()
    {
        if (gameObject.transform.childCount <= 2)
        {
            Instantiate(m_fireworks, m_spawnParticle.position, m_spawnParticle.rotation, gameObject.transform);
        }
    }
}
