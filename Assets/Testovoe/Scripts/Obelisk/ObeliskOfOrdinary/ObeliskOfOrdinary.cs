﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObeliskOfOrdinary : AObelisk
{
    [SerializeField] private Trigger[] m_Triggers;

    private void OnTriggerStay(Collider other)
    {
        if (!IsActivate)
        {
            if (other.tag == "Player")
            {
                var allPassed = true;
                foreach (Trigger trigger in m_Triggers)
                {
                    if (!trigger.IsPassed)
                    {
                        allPassed = false;
                        break;
                    }
                }
                if (allPassed)
                {
                    IsActivate = true;
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        foreach (Trigger trigger in m_Triggers)
        {
            trigger.IsFirst = false;
            trigger.IsPassed = false;
        }
    }

}
