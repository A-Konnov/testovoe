﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    [SerializeField] private Trigger[] m_Triggers;

    public bool IsPassed { get; set; }
    public bool IsFirst { get; set; }

    private void Start()
    {
        IsPassed = false;
        IsFirst = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            DefineFirst();
            WentBack();
            WentAround();
        }
    }

    private void WentAround()
    {
        if (IsFirst)
        {
            var triggersEnabled = 0;
            for (int i = 0; i < m_Triggers.Length; i++)
            {
                if (m_Triggers[i].IsPassed)
                {
                    triggersEnabled++;
                }
            }
            if (triggersEnabled >= m_Triggers.Length - 1)
            {
                IsPassed = true;
            }
        }
        else
        {
            IsPassed = true;
        }
    }

    private void WentBack()
    {
        if (IsPassed == true && IsFirst != true)
        {
            foreach (Trigger otherTrigger in m_Triggers)
            {
                otherTrigger.IsPassed = false;
                otherTrigger.IsFirst = false;
            }
            IsFirst = true;
        }
    }

    private void DefineFirst()
    {
        var haveFirst = false;
        foreach (Trigger trigger in m_Triggers)
        {
            if (trigger.IsFirst)
            {
                haveFirst = true;
                break;
            }
        }
        if (!haveFirst)
        {
            IsFirst = true;
        }
    }
}
