﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareWorship : AObelisk
{
    [SerializeField] private float m_timeToStand = 10;

    private float m_currTime = 0;
    private Vector3 m_currPosition;
    private Vector3 m_lastPosition = new Vector3(0, 0, 0);

    private void OnTriggerStay(Collider other)
    {
        if (!IsActivate)
        {
            if (other.tag == "Player")
            {
                m_currPosition = other.gameObject.transform.position;

                if (m_currPosition == m_lastPosition)
                {
                    m_currTime = m_currTime + Time.deltaTime;
                    if (m_currTime >= m_timeToStand)
                    {
                        IsActivate = true;
                    }
                }
                else
                {
                    m_currTime = 0;
                }
                m_lastPosition = m_currPosition;
            }
        }
    }
}
