﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    [Header("Property")]
    [SerializeField] private float m_speedMovement = 0.3f;

    [Header("Reference")]
    [SerializeField] private Animator m_animator;
    private float m_horizontalSpeed = 0;
    private float m_verticalSpeed = 0;
    private SwordBlow m_swordBlow;

    private void Start()
    {
        m_animator = GetComponent<Animator>();
        m_swordBlow = GetComponent<SwordBlow>();
    }

    private void Update()
    {
        m_verticalSpeed = Input.GetAxis("Vertical");
        m_horizontalSpeed = Input.GetAxis("Horizontal");
        m_animator.SetFloat("Speed", m_verticalSpeed);
        m_animator.SetFloat("Direction", m_horizontalSpeed);
    }
}
