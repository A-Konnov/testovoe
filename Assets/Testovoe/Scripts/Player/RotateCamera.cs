﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    [SerializeField] public float m_minimumVert = -30.0f;
    [SerializeField] public float m_maximumVert = 30.0f;
    [SerializeField] public float m_speedRotate = 9.0f;
    private float m_angleVertikal = 0;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        m_angleVertikal -= Input.GetAxis("Mouse Y") * m_speedRotate * Time.deltaTime;
        m_angleVertikal = Mathf.Clamp(m_angleVertikal, m_minimumVert, m_maximumVert);
        float rotationY = transform.localEulerAngles.y;
        transform.localEulerAngles = new Vector3(m_angleVertikal, rotationY, 0);
    }
}
