﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayer : MonoBehaviour
{
    [SerializeField] public float m_speedRotate = 9.0f;

    void Update()
    {
        transform.Rotate(0, Input.GetAxis("Mouse X") * m_speedRotate * Time.deltaTime, 0);
    }
}
