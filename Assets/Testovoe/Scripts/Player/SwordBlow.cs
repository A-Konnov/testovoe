﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordBlow : MonoBehaviour
{
    [Header("Reference")]
    [SerializeField] private Animator m_animator;
    private int m_random;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_random = Random.Range(1, 3);

            if (m_random == 1)
            {
                m_animator.SetTrigger("SwordBlow01");
            }
            else
            {
                m_animator.SetTrigger("SwordBlow02");
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            m_animator.SetTrigger("AttackStrong");
        }
    }
}
