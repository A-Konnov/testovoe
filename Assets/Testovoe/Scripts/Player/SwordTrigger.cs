﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SwordTrigger : MonoBehaviour
{
    [SerializeField] private Transform m_ponit0;
    [SerializeField] private Transform m_ponit1;
    [SerializeField] private float m_radius = 5f;

    //this method is called from animation
    public void IsOverlapObelisk()
    { 
        var colliders = Physics.OverlapCapsule(m_ponit0.position, m_ponit1.position, m_radius);

        foreach (var collider in colliders)
        {
            if (collider.gameObject.CompareTag("Vase of Eternity"))
            {
                collider.gameObject.GetComponent<VaseOfEternity>().IsActivate = true;
            }
        }
    }
}
